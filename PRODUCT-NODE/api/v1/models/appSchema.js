const Joi = require('@hapi/joi')

module.exports = appSchema = Joi.object({
    appname:Joi.string().min(3).required(),
    appurl: Joi.string().uri(),   
})
options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};