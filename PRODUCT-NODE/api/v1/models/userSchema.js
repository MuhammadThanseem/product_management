const Joi = require('joi')

module.exports = userSchema = Joi.object({
    first_name: Joi.string().min(3).required(),
    last_name: Joi.string().min(3).required(),
    phone1: Joi.string().length(10).pattern(/^[0-9]+$/).required(),
    phone2: Joi.string().length(10).pattern(/^[0-9]+$/).required(),
    email: Joi.string().email({minDomainSegments: 2}).required(),
    password: Joi.string().min(4).max(15).required().not(""),
})
 options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};


