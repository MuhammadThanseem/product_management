const { ObjectID } = require('mongodb');
var dbConfig = require('../config/config.json'),
    crypto = require('crypto'),
    db = require('../config/dbConfig'),
    MongoClient = require('mongodb').MongoClient;
var salt = crypto.randomBytes(16).toString('hex');
MongoClient.connect(dbConfig.dbUrl, async (err, db) => {

    console.log('Seeding Users ......');
    var body = {
        FIRST_NAME: 'admin',
        LAST_NAME: 'PRODUCT',
        PHONE_NUMBER: '9526069529',
        EMAIL: 'admin@gmail.com',
        USER_STATUS: true,
        DELETE_STATUS:false,
        SALT: salt,
        PASSWORD: generatePassword('admin'),
        PRIVILEGE: -1,

    }
    var userCollection = db.collection(dbConfig.USER);
    if (!userCollection) {
        console.log('Collection is not defined in database')
    }
    let user = await db.collection(dbConfig.USER).findOne({$or: [
            {EMAIL: body.EMAIL},
            {COL_MOB: body.MOB},
        ]});
    if (user) {
        console.log('user already exists')
    } else {
        db.collection(dbConfig.USER).insertOne(body, (err, result) => {
            if (err) {
                console.log('ERROR = > ' + err)
            } else {
                console.log('User Seed Successfully Completed')
            }
        })
    }


})

function generatePassword(pas) {
    var password = crypto.pbkdf2Sync(pas, salt, 1000, 64, 'sha512').toString('hex');
    return password;

};
