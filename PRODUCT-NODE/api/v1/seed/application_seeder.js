var dbConfig = require('../config/config.json'),
    db = require('../config/dbConfig'),
    ObjectID = require('mongodb').ObjectID,
    MongoClient = require('mongodb').MongoClient;

    MongoClient.connect(dbConfig.dbUrl, async (err, db) => {
        console.log('Seeding Application ......');
        db.collection(dbConfig.APPLICATIONS).findOne({
            $and: [
                {APP_NAME: 'PRODUCT'},
            ]
        }).then((resultFind) => {
            if (!resultFind) {
                db.collection(dbConfig.APPLICATIONS).insertOne({
                    _id: ObjectID('60f547696bdf8c3ee07dd888'),
                    LOGIN_DATE: new Date(),
                    APPLICATION_NAME: 'PRODUCT',
                    APPLICATION_LOGO_URL:'',
                    APPLICATION_FAV_ICON_URL:'',
                    VERIFICATION_REQUIRED: true,
                    VERIFICATION:{
                        MOBILE_NUMBER:true,
                        EMAIL: true,
                    },
                    APP_URL:'http://localhost:8200',
                    FIREWALL_ENABLED: true,
                    CONCURRENT_LOGIN:-1,
                })
                .then((result) => {
                    console.log('Success')
                }).catch((err) => {
                    console.log('ERROR:=>')
                    console.log(err)
                })
            } else {
                console.log('Application already exists');
            }
        }).catch((errFind) => {
            console.log(errFind)
        })
    
    })