var express = require('express');
var router = express.Router();
var userHelper=require('../HELPER/user-helper');
var isValidApplication = require('../middleware/isValidApplication');
var isValidSession = require('../middleware/isValidSession');
var isValidUser = require('../middleware/isValidUser');
var isValidPrivilege = require('../middleware/isValidPrivilege');

router.post('/create',[isValidApplication], async(req, res) => {
    let body=req.body;
    // body._id = req.params.id;
    // body.appID = req.APP._id;
    // body.sessionID = req.SESSION._id;
    // body.appData = req.APP;
    // body.userPrivilege = req.PRIVILEGE.PRIVILEGES.USER;
    let userResponse=await userHelper.createUser(body);
    res.json(userResponse);
});

router.post('/list',[isValidApplication, isValidUser],async (req,res,next)=> {
    let body = req.body;
    body.appID = req.APP._id;
    body.userPrivilege = req.PRIVILEGE.PRIVILEGES.USER;
    let listUsers=await userHelper.getUserData(body);
    res.json(listUsers);
});

router.delete("/delete/:id", [isValidApplication, isValidUser], async(req, res) => {
    let user_id = req.params.id
    let body = req.body;
    body.userPrivilege = req.PRIVILEGE.PRIVILEGES.USER;
    console.log('[inside delete user]user_id',user_id);
    let deleteUser=await userHelper.deleteUser(user_id, body);
    console.log(deleteUser, 'deleteUser');
    res.json(deleteUser);

});

module.exports = router;