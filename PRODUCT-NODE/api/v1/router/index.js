var express = require('express');
var router = express.Router();

router.use('/users', require('./user'));
router.use('/auth', require('./auth'));
router.use('/product',require('./product'));

module.exports = router;