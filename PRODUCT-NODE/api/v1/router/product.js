var express = require('express');
var router = express.Router();
var productHelper = require('../HELPER/product-helper');
var isValidApplication = require('../middleware/isValidApplication');
var isValidSession = require('../middleware/isValidSession');
var isValidUser = require('../middleware/isValidUser');

router.post('/create',[isValidApplication,isValidSession,isValidUser], async (req, res) => {
    let body = req.body;
    body.privilege = req.USR.privilege;
    let prodResponse = await productHelper.createProduct(body);
    res.json(prodResponse);
});

router.post('/list', [isValidApplication,isValidSession,isValidUser], async (req, res, next) => {
    let body = req.body;
    body.privilege = req.USR.privilege;
    let listOrganization = await productHelper.getProductData(body);
    res.json(listOrganization);
});

router.delete("/delete/:id", [isValidApplication,isValidSession,isValidUser], async (req, res) => {
    let id = req.params.id
    let body = req.body;
    body.privilege = req.USR.privilege;
    let deleteProduct = await productHelper.deleteProduct(id, body);
    res.json(deleteProduct);

});

router.get("/getDetails/:id", [isValidApplication,isValidSession,isValidUser], async (req, res) => {
    let id = req.params.id;
    let body = req.body;
    body.privilege = req.USR.privilege;
    console.log('inside list details of a Product: prod_id', id);
    let editProduct = await productHelper.getProductDetailsUsingId(id,body);
    res.json(editProduct);
});

router.put("/update/:id", [isValidApplication,isValidSession,isValidUser], async (req, res) => {
    let id = req.params.id
    let body = req.body;
    body.privilege = req.USR.privilege;
    let updateProduct = await productHelper.updateProduct(id, body);
    res.json(updateProduct);
});

module.exports = router;