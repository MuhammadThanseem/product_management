var express = require('express');
const _auth = require('../HELPER/auth-helper');
var router = express.Router();
var authHelper=require('../HELPER/user-helper');
var isValidApplication = require('../middleware/isValidApplication');
const { message } = require('../models/userSchema');
var nodemailer = require("nodemailer");


/******************************* */
router.post('/doLogin',[], async (req, res, next)=> {
  let body=req.body;

  let authDone =await _auth.validateUser(body);
  console.log(authDone, '[isAuthentication Response] inside auth.js doLogin');
  await _auth.createLoginlog(body, authDone);
  if(authDone.isAuthenticate == false){
    res.status(401).json(authDone);
  } else {
    res.status(200).json(authDone);
  }
});

router.post('/doLogOut',[isValidApplication], async (req, res, next) => {
  const body=req.body;
  body.appID = req.APP._id;
  let doLogout =await _auth.deleteSession(body);
  res.json(doLogout);
});

module.exports = router;