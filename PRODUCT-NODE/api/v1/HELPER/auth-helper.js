let jwt = require("jsonwebtoken"),
  db = require("../config/dbConfig"),
  ObjectID = require("mongodb").ObjectID,
  bcrypt = require("bcrypt"),
  _user = require("./user-helper"),
  dbConfig = require("../config/config.json"),
  crypto = require('crypto'),
  geoip = require('geoip-lite');
_auth = {

  validateUser: async (data) => {
    console.log('[EMAIL AND PASSWORD ENTERED]', data)
    let user = await db
      .get()
      .collection(dbConfig.USER)
      .findOne({
        EMAIL: data.FormData.user_name,
      });
    console.log('[USER LOGIN DETAILS] according to email', user);
    if (!user) {
      return {
        isAuthenticate: false,
        message: "Unauthorized Access",
      };
    } else if (await _user.verifyPassword(user.PASSWORD, data.FormData.password, user.SALT)) {

      if (data.firewallEnable == true) {

      } else {
        console.log('##############');
        try {
          let index = await db
            .get()
            .collection(dbConfig.SESSION)
            .createIndex({ CREATED_ON: 1 }, { expireAfterSeconds: 600 });
        } catch (e) {
          console.log("Indexing Error");
        }

        let token = await _auth.getAccessToken(user);
        var location = geoip.lookup(data.ipAddress);
      
         

        let session = await db.get().collection(dbConfig.SESSION).insertOne({

          USER_ID: ObjectID(user._id),
          LOGIN_KEY: '',
          TOKEN: token,
          LOGIN_TIME: new Date(),
        });

        if (session) {
          console.log('[****SESSION CREATED****]');
          let sessionLog = await db.get().collection(dbConfig.SESSION_LOG).insertOne({
            _id: ObjectID(session.insertedId),
            USER_ID: ObjectID(user._id),
            LOGIN_KEY: '',
            TOKEN: token,
            LOGIN_TIME: new Date(),
          });

          return {
            isAuthenticate: true,
            token: token,
            session_id: session.insertedId,
          };
        } else {
          return {
            isAuthenticate: false,
            message: "Unauthorized Access",
          };
        }
      }
    } else {
      return {
        isAuthenticate: false,
        message: "Unauthorized Access",
      };
    }
  },
  createLoginlog: async (data, authDone) => {
    if (authDone.isAuthenticate == true) {
      console.log('[****USER AUTHENTICATED****]inside login log');
      await db
        .get()
        .collection(dbConfig.LOGIN_LOG)
        .insertOne(_auth.arrangeSuccessLoginlog(data, authDone));
    } else {
      await db
        .get()
        .collection(dbConfig.LOGIN_LOG)
        .insertOne(_auth.arrangeFailedLoginlog(data));
    }
  },
  arrangeSuccessLoginlog: (data, authDone) => {
    return {
      USER_NAME: data.FormData.user_name,
      PASSWORD: data.FormData.password,
      LOGIN_STATUS: 'Success',
      APPLICATION_ID: ObjectID(data.appID),
      SESSION_LOG_ID: ObjectID(authDone.session_id),
      LOGIN_DATE: new Date(),
    };
  },
  arrangeFailedLoginlog: (data) => {
    return {
      USER_NAME: data.FormData.user_name,
      PASSWORD: data.FormData.password,
      LOGIN_STATUS: 'Failed',
      FAILED_REASON: 'Incorrect username or password',
      APPLICATION_ID: ObjectID(data.appID),
      LOGIN_DATE: new Date(),
    };
  },
  getAccessToken: async (user) => {
    console.log('[TOKEN CREATING.......]');
    var token = jwt.sign(user, 'shhhhh');
    return token;
  },
  VerifyToken: async (token) => {
    if (await _auth.checkTokenAvailable(token)) {
      var decoded = jwt.verify(token, 'shhhhh');
      if (decoded) {
        var usr = await _user.getUserDetailsUsingId(decoded._id)
        console.log('inside decoded');
        console.log(usr);
        if (usr) {
          return usr;
        } else {
          return null;
        }
      } else {
        // return null;
        return {
          isAuthenticate: false,
          message: "Unauthorized Access",
        };
      }
    } else {
      return null;
    }
  },
  checkTokenAvailable: async (token) => {
    console.log('inside check Token Available')
    let data;
    let validToken = await db
      .get()
      .collection(dbConfig.SESSION)
      .updateOne({ TOKEN: token }, { $set: { CREATED_ON: new Date() } });
    data = (validToken.result.nModified > 0) ? token : null;
    return data;
  },
  deleteSession: async (body) => {
    try {
      db.get().collection(dbConfig.SESSION).deleteOne(
        {
          TOKEN: body.token,
          // APPLICATION_ID: ObjectID(body.appID),
          // USER_ID: ObjectID(body.user_id),
        });
      return {
        isLogout: true,
        message: "Valid Username and Password",
      };
    } catch (e) {
      console.log('[DELETE SESSION ERROR]', e);
    }
  },

  isValidPrivilege: (privilegeId) => {
    return new Promise(async (resolve, reject) => {
      try {
        let privilege = await db.get().collection(dbConfig.PRIVILEGE).aggregate([
          // {
          //     // $match: { APPLICATION_ID: ObjectID(data.appID) },
          // },
          {
              $unwind:"$PRIVILEGES"
          },
          {
              $project: {
                  name: "$NAME",
                  master_id:"$PRIVILEGES.PRIVLEGE_MASTER_ID",
                  view:"$PRIVILEGES.ROLES.VIEW",
                  write:"$PRIVILEGES.ROLES.WRITE",
                  edit:"$PRIVILEGES.ROLES.EDIT",
                  delete:"$PRIVILEGES.ROLES.DELETE",
              }
          }
      ]).toArray();
        if (privilege) {
          resolve(privilege);
        } else {
          reject(null);
        }
      } catch (e) {
        reject(null);
      }
    })
  },
};
module.exports = _auth;