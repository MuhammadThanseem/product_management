let db = require("../config/dbConfig"),
    ObjectID = require("mongodb").ObjectID,
    dbConfig = require("../config/config.json"),

    _product = {

        /******
        * Create Product Data
        * Created By MUHAMMAD THANSEEM C
        * Created On 19-SEP-21
       ******/
        createProduct: async (data) => {
            console.log('data in create product', data);
            try {
                if(data.privilege == -1){
                let newProduct = await db
                    .get()
                    .collection(dbConfig.PRODUCT)
                    .insertOne(await _product.arrangeProductForCreation(data));
                if (newProduct) {
                    return {
                        isCreated: true,
                        message: "Product Created Successfully",
                    };
                } else {
                    return {
                        isCreated: false,
                        message: "Internal Server Error Please Try After Some Time",
                    };
                }
            }else{
                return {
                    isCreated: false,
                    message: "No Privilege To Create Data",
                };
            }
            } catch (e) {
                return { isCreated: false, message: e.message };
            }
        },

        /******
        * Update Product Data
        * Created By MUHAMMAD THANSEEM C
        * Created On 19-SEP-21
       ******/

        updateProduct: async (id, data) => {
            try {
                if(data.privilege == -1){
                console.log('updating')
                let updateProduct = await db
                    .get()
                    .collection(dbConfig.PRODUCT)
                    .updateOne(
                        {
                            _id: ObjectID(id)

                        },
                        await _product.arrangeProductForUpdate(data)
                    );
                if (updateProduct) {
                    return {
                        isUpdated: true,
                        message: "Product Successfully Updated",
                    };
                } else {

                    return {
                        isUpdated: false,
                        message: "Internal Server Error Please Try After Some Time",
                    };
                }
            }else{
                return {
                    isCreated: false,
                    message: "No Privilege To Update Data",
                };
            }
            } catch (e) {
                return { isUpdated: false, message: e.message };
            }
        },

        /****** 
        * Arrange Data For Product Creation
        * Created By MUHAMMAD THANSEEM C
        * Created On 19-SEP-21
       ******/

        arrangeProductForCreation: async (data) => {
            console.log('data in arrangeForCreation', data)
            return {
                PRODUCT_NAME: data.name,
                DESCRIPTION: data.description,
                CATEGORY: data.category,
                PRICE: data.price,
                STATUS: true
            }
        },

        /****** 
        * Arrange Data For Product Updation
        * Created By MUHAMMAD THANSEEM C
        * Created On 19-SEP-21
       ******/

        arrangeProductForUpdate: async (data) => {
            console.log('data in arrangeForUpdation', data)
            return {
                $set: {
                    PRODUCT_NAME: data.name,
                    DESCRIPTION: data.description,
                    CATEGORY: data.category,
                    PRICE: data.price,
                    STATUS: true
                }
            }
        },

        /****** 
         * List Product data
         * Created By MUHAMMAD THANSEEM C
         * Created On 19-SEP-21
        ******/

        getProductData: async (data) => {
            try {

                let productArray = await db
                    .get().collection(dbConfig.PRODUCT).aggregate([
                        {
                            $project: {
                                _id: "$_id",
                                name: "$PRODUCT_NAME",
                                description: "$DESCRIPTION",
                                category: "$CATEGORY",
                                price: "$PRICE",
                                status: "$STATUS"
                            }
                        }
                    ])
                    .toArray()
                return { data: productArray }

            } catch (e) {
                console.log(e)
            }
        },

        /****** 
         * Delete Unneccessary Product Data
         * Created By MUHAMMAD THANSEEM C
         * Created On 19-SEP-21
        ******/

        deleteProduct: async (id,data) => {
            try {
                if(data.privilege == -1){
                let deleteProduct = await db
                    .get()
                    .collection(dbConfig.PRODUCT)
                    .deleteOne(
                        { _id: ObjectID(id) });
                if (deleteProduct) {
                    return {
                        isDelete: true,
                        message: "Product Successfully Deleted",
                    };
                } else {
                    return {
                        isDelete: false,
                        message: "Internal Server Error Please Try After Some Time",
                    };
                }
            }else{
                return {
                    isCreated: false,
                    message: "No Privilege To Create Data",
                };
            }
            } catch (e) {
                return { isDelete: false, message: e.message };
            }
        },
        /****** 
        * List details of a specific Product using id 
        * Created By MUHAMMAD THANSEEM C
        * Created On 19-SEP-21
       ******/

        getProductDetailsUsingId: async (id,data) => {
            if(data.privilege == -1){
            let getProduct = await db
                .get()
                .collection(dbConfig.PRODUCT)
                .aggregate([
                    {
                        $match: { _id: ObjectID(id) },
                    },
                    {
                        $project: {
                            _id: "$_id",
                            name: "$PRODUCT_NAME",
                            description: "$DESCRIPTION",
                            category: "$CATEGORY",
                            price: "$PRICE",

                        },
                    },
                ])
                .toArray();
            return getProduct[0];
            }else{
                return {
                    isCreated: false,
                    message: "No Privilege To Create Data",
                };
            }
        },
    }

module.exports = _product