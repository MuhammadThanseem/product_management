let db = require("../config/dbConfig"),
    ObjectID = require("mongodb").ObjectID,
    bcrypt = require("bcrypt"),
    dbConfig = require("../config/config.json"),
    crypto = require('crypto'),
    _user = {


        createUser: async (data) => {
            ;
            try {
                let usrExist = await db.get().collection(dbConfig.USER)
                    .findOne({
                        $or: [
                            { PHONE_NUMBER: data.mobile },
                            { EMAIL: data.email },
                        ]
                    });
                if (usrExist) {
                    return {
                        isCreated: false,
                        message: "Phone Number or Email already Exist",
                    };
                } else {
                    let ext = Date.now();
                    let verification_code = randomCodeGenerator(12) + ext;
                    let newUser = await db
                        .get()
                        .collection(dbConfig.USER)
                        .insertOne(await _user.arrangeUserForCreation(data, verification_code));
                    if (newUser) {
                        return {
                            isCreated: true,
                            message: "User Create Success",
                            verification_code
                        };
                    } else {
                        return {
                            isCreated: false,
                            message: "Internal Server Error Please Try After Some Time",
                        };
                    }
                }
            } catch (e) {
                return { isCreated: false, message: e.message };
            }
        },
        arrangeUserForCreation: async (data, verification_code) => {
            console.log(data)
            var salt = await crypto.randomBytes(16).toString('hex');
            let newPass = await _user.generatePassword(data.password, salt, verification_code);
            return {
                NAME: data.name,
                PHONE_NUMBER: data.mobile,
                EMAIL: data.email,
                USER_NAME: data.email,
                SALT: salt,
                PASSWORD: newPass,
                USER_STATUS: true,
                PRIVILEGE: 0,
                APPLICATION_ID: ObjectID(data.appID),
                CREATED_ON: new Date(),
                VERIFICATION_CODE: verification_code
            };
        },



        generatePassword: async (pas, salt) => {
            var password = crypto.pbkdf2Sync(pas, salt, 1000, 64, 'sha512').toString('hex');
            return password;
        },

        verifyPassword: (dbPassword, inputPassword, salt) => {

            if (dbPassword == crypto.pbkdf2Sync(inputPassword, salt, 1000, 64, 'sha512').toString('hex')) {
                return true;
            } else {
                return false;
            }
        },
        getUserData: async (data) => {
            try {
                if (data.userPrivilege.VIEW == true) {
                    var skip = Number(data.pageSize) * Number(data.pageNumber - 1);
                    var sort = {};
                    //sort[data.sort.sortName] = Number(data.sort.sortValue);
                    var condition = {};
                    let count = await db
                        .get()
                        .collection(dbConfig.USER)
                        .find()
                        .count();
                    let UserArray = await db
                        .get().collection(dbConfig.USER).aggregate([
                            {
                                $match: { APPLICATION_ID: ObjectID(data.appID) },
                            },
                            {
                                $project: {
                                    user_fname: '$FIRST_NAME',
                                    user_lname: '$LAST_NAME',
                                    user_phone: '$PHONE_NUMBER',
                                    user_phone2: '$PHONE2_NUMBER',
                                    user_email: '$EMAIL',
                                    user_address: '$ADDRESS',
                                    user_status: '$USER_STATUS',

                                }
                            }
                        ])
                        .skip(skip)
                        .limit(data.pageSize)
                        .toArray()
                    return { data: UserArray, count: count };
                } else {
                    return null;
                }
            } catch (e) {
                console.log(e)
            }
        },
        deleteUser: async (id, body) => {
            try {
                if (body.userPrivilege.DELETE == true) {
                    let deleteUser = await db
                        .get()
                        .collection(dbConfig.USER)
                        .deleteOne(
                            { _id: ObjectID(id) });
                    if (deleteUser) {
                        return {
                            isDelete: true,
                            message: "User Successfully Deleted",
                        };
                    } else {
                        return {
                            isDelete: false,
                            message: "Internal Server Error Please Try After Some Time",
                        };
                    }
                } else {
                    return {
                        isDelete: false,
                        message: "No Privilege to Delete",
                    };
                }
            } catch (e) {
                return { isDelete: false, message: e.message };
            }
        },

        /****** 
    * List details of a specific user using id 
    * Created By MUHAMMAD THANSEEM C
    * Created On 19-SEP-21
   ******/

    getUserDetailsUsingId: async (id) => {
        console.log('user id in edit helper', id)
        let getUser = await db
            .get()
            .collection(dbConfig.USER)
            .aggregate([
                {
                    $match: { _id: ObjectID(id) },
                },
                {
                    $project: {
                        name: '$NAME',
                        email: '$EMAIL',
                        mobile: '$PHONE_NUMBER',
                        privilege:"$PRIVILEGE"
                    }
                },
            ])
            .toArray();
        return getUser[0];
    },

    };
function randomCodeGenerator(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
module.exports = _user;
