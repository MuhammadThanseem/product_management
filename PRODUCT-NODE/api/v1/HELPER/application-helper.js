let db = require("../config/dbConfig"),
    ObjectID = require("mongodb").ObjectID,
    dbConfig = require("../config/config.json"),
    _app = {

isValidApp: (url) => {
    return new Promise(async (resolve, reject) => {
        try {
            let app = await db.get().collection(dbConfig.APPLICATIONS).findOne({APP_URL:url});
            if(app){
                resolve(app);
            } else {
                reject(null);
            }
        } catch (e) {
            reject(null);
        }
    })
},
};

module.exports = _app;