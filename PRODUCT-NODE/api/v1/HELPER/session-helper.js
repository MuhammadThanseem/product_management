let db = require("../config/dbConfig"),
    ObjectID = require("mongodb").ObjectID,
    dbConfig = require("../config/config.json"),
    _session = {

sessionValidate: (token) => {
    return new Promise(async (resolve, reject) => {
        try {
            let sessionData = await db.get().collection(dbConfig.SESSION_LOG).findOne({TOKEN:token});
            if(sessionData){
                resolve(sessionData);
            } else {
                reject(null);
            }
        } catch (e) {
            reject(null);
        }
      })
},
};

module.exports = _session;