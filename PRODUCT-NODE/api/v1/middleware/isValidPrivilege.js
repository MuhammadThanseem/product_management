// const isValidUser = require('./isValidUser');
let _AUTH=require('../HELPER/auth-helper');

module.exports = async function isValidPrivilege(req, res, next) {
    try {
            let authorization = req.headers.authorization;
            let token = authorization.split(' ')[1];
            let userData=await _AUTH.VerifyToken(token);
            if(userData){
            let privilege = await _AUTH.isValidPrivilege(userData.user_privilege);
            if (privilege) {
                console.log('[PRIVILEGE DATA FETCH] success', privilege);
                req.PRIVILEGE = privilege;
                next();
            } else {
                console.log('[PRIVILEGE DATA FETCH] Failed');
                console.log('Validation Data : ','Invalid URL');
                res.send(401, "Unauthorized");
            }
        }
    } catch (e) {
        console.log('[PRIVILEGE VALIDATION] Catch Error ');
        console.log(`Error : ${e}`);
        res.send(401, "Unauthorized");
    }
};