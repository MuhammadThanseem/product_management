let _AUTH=require('../HELPER/auth-helper');
module.exports = async function isValidUser(req, res, next) {
    try{
        let authorization = req.headers.authorization;
        let token = authorization.split(' ')[1];
        let tokenValidation=await _AUTH.VerifyToken(token)
        if(tokenValidation){
            console.log('[USER VALIDATION] success');
            req.USR=tokenValidation;
            req.UserID=tokenValidation._id;
            next();
        } else {
            console.log('[USER VALIDATION] Failed');
            console.log('Validation Data : ','Invalid TOKEN');
            // res.send(401, "Unauthorized");
            res.status(401).json(tokenValidation);
        }
    } catch(e){
        console.log('[USER VALIDATION] Catch Error ');
        console.log(`Error : ${e}`);
        res.send(401, "Unauthorized");
    }
};