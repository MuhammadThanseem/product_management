let _APP = require('../HELPER/application-helper');
module.exports = async function isValidApplication(req, res, next) {
    try {
            let url = req.headers.origin.toString();
            let app = await _APP.isValidApp(url);
            if (app) {
                console.log('[APPLICATION VALIDATION] success');
                req.APP = app;
                next();
            } else {
                console.log('[APPLICATION VALIDATION] Failed');
                console.log('Validation Data : ','Invalid URL');
                res.send(401, "Unauthorized");
            }
    } catch (e) {
        console.log('[APPLICATION VALIDATION] Catch Error ');
        console.log(`Error : ${e}`);
        res.send(401, "Unauthorized");
    }
};


