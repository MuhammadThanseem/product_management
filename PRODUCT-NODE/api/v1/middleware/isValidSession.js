let _SESSION = require('../HELPER/session-helper');
module.exports = async function isValidSession(req, res, next) {
    try{
        let authorization = req.headers.authorization;
        let token = authorization.split(' ')[1];
        let sessionValidation = await _SESSION.sessionValidate(token);
        if(sessionValidation){
            console.log('[SESSION VALIDATION] success');
            req.SESSION = sessionValidation;
            next();
        } else {
            console.log('[SESSION VALIDATION] Failed');
            console.log('Validation Data : ','Invalid TOKEN');
            res.send(401, "Unauthorized");
        }
    } catch(e){
        console.log('[SESSION VALIDATION] Catch Error ');
        console.log(`Error : ${e}`);
        res.send(401, "Unauthorized");
    }
};